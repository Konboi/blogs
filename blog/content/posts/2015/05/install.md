+++
date = "2015-05-06T18:16:32+09:00"
title = "hugoを使ってブログ構築"
+++

# hugo を使ってblog構築

## Install hugo

```
go get -u -v github.com/spf13/hugo
```

でhugoのインストールはOK

## Init BLOG

```
hugo new site blog
```

するとこんな感じでブログの雛形が完成


```
$ tree blog

blog
├── archetypes
├── config.toml
├── content
├── data
├── layouts
└── static
```

## Edit config.toml

```
emacs config.toml
```

で適宜設定を自分のblogの設定に書き換える

## Write first blog

ということで準備が整ったので最初の記事(この記事)を書いてみる


```
hugo new install.md
```

すると `blog/content/install.md` というファイルが出来るのでそれを編集する


## Hugo server

編集がある程度終わったので､


```
hugo server
```

で起動してみる起動すると1313ポートで起動したのでアクセスしてみる

が何も表示されない｡

調べてみるとthemeをインストールする必要があるらしい


## Install theme

```
git clone --recursive https://github.com/spf13/hugoThemes.git themes
```

でthemeがインストールされる｡
ただ､テーマ全部いらないのでさらっと見てよさそうな `purehugo` をインストールしてみる


```
mkdir themes
git@github.com:dplesca/purehugo.git
```

## Hugo server

themeのインストールが無事終わったので起動してみる

```
hugo server --theme=purehugo
```

改めて `localhost:1313` にアクセスしてみる｡

無事表示された

## まとめ

hugoのインストールから起動まですんなりいった｡
テーマもそれなりに揃っている印象を受けたので今の所オススメです｡

次はブログを書いたらどうやってサーバーにdeployするかを考えようと思う｡
